TO FIX/confirm

Most Spanish banners requiring two line subheads are a hard read. We should 
consider shortening the message for digital attention span.

Do we need to run New Hampshire? Meeting notes said no, but they were in the 
spreadsheet, please confirm.

will we have a new logo for INdiana?

logo for arkansas sits quite high when cutting off raspberry circle at top.
-in general we used un-sanctioned lockups for all banners -- are we cleared to do so 
again this year? If not, please replace all logos with sanctioned lockups. they will
not fit the layouts the same (reckoning we'll re-do some sizes layouts quite differently.)

possibly have to create new 4th version to not have smaller subhead or smaller headline, 
but still have smaller button. Trying not to have to do 3 separate additional versions

the underlines will need to be replaced with something temporarily because script that 
puts the group of gifs into folders that can run a script to make a PDF in photoshop uses 
underlines as the spitting off point. after proofs are created we can put back underlines. 
if this can be done as part of the script it would save a lot of time.


fixed history

the letter Q has descenders even in the capital letter, and will force some layouts to 
horizontally center the headline on 2-line headlines with all caps.
(the centering method we used to center the button that normally
does not kick in for the headline). So watch out for centered headlines that are 2 line
and to fix this I spread out the extra height needed by reducing font size and leading
and also making the "txt_2" GUIDE a little taller... careful not to vertically center 
to where the Q descender overlaps the legal code below headline.

2-line psd is not playing well with 2-line hit list
 [fixed, NH layers were not copied over at first-error can't find logo_layers or similar]

create smaller buttons for needed overflow buttons which also has 
smaller headline and subhead to fit
 [done, FIT version]

create smaller headline version with 1 line psd
 [done, 2 line version] 

why is there a dash at -_00 in some gifs named in first batch? Did removing the 00 also 
remove the dash (run default js and template again
 [fixed, remove all spaces from state, theme, and other data content that ends up as file 
 name-spaces create dashes when file is made] 

when changing something, always store good GIFS before running next batch in case revert 
is needed.
(exception: i have to rename the first full batch to drop 00 (go back and clean data 
sheet)

AFTER the different templates are run and checked, quarantine each batch so they wont be 
overwritten by later runs. These backup files can be kept away from repo.


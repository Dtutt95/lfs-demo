// be sure to change AMB14/AMB15 data file, and edit hard coded txt_code this file
// and on global.js export file name label

#include "json2.js";
#include "globals.js";

// OUTPUT FOLDER path, and current size for file name
   var outputFolder = "/output_728x90"; var currentSize = "728x90_";


// select the correct data to override what's in the global file
// run files with matching templates for 728 size in this order:
// var dataFile = File(scriptFilePath + "/728_AMB16_data_normal_1.json");
// var dataFile = File(scriptFilePath + "/728_AMB16_data_FIT_2.json");
var dataFile = File(scriptFilePath + "/728_AMB16_data_centered_3.json");


// TIME TO KICK BACK AND TAKE A SNOOZE.
loadData();


// BANNER SPECIFIC ALTERATIONS
function makeAlterations() {

	var newText, editLayer;



	// TURN LOGO ON
	var logoLayer = currentState + '_' + currentBanner.language;
    
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 4 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

	
	app.activeDocument.layerSets["group_logos"].layerSets[logoLayer].visible = true;



	// group_txt_1
	if (currentBanner.language === "EN") {
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 1 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_EN"].visible = true;

	} else {

            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 1 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_ES"].visible = true;

	}
//     NO LONGER NEED to populate TEXT 1, one of two static layers are turned on above!	
//     populate txt 1	
//	if (editLayer.textItem && editLayer.textItem.contents !== "") {

//		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_1);
//		editLayer.textItem.contents = newText;
//		editLayer.textItem.convertToShape();
//
//	}


// ***** current dilemma- this is not v-centering txt 2 nor 3, but did v-center button txt, why?
// txt 2 is paragraph text, txt 3 is point text, and btn is paragraph txt

	// group_txt_2
	editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
    
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 2 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );


		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_2);
		editLayer.textItem.contents = newText;
		editLayer.textItem.convertToShape();

    
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 2 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

	

			var txt_2_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_2_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_2_bounds 	= editLayer.bounds;
			var txt_2_X 		= txt_2_bounds[0].value;
			var txt_2_Y 		= txt_2_bounds[1].value;
		}

		// txt_2_guide position / dimensions
		    // =======================================================
            // for good measure, re-select frame 2
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 2 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );


		editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2_guide"];
		
		    
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 2 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );
			
			var txt_2_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_2_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_2_guide_bounds 	= editLayer.bounds;
			var txt_2_guide_X 		= txt_2_guide_bounds[0].value;
			var txt_2_guide_Y 		= txt_2_guide_bounds[1].value;

		// check if txt_2 is taller than guide
		if (txt_2_Height > txt_2_guide_Height) {
			editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];
			
			// =======================================================
            // for good measure, re-select frame 2
                var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 2 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );
			
			// get new scale percentage
			newTextScale = Math.floor(txt_2_guide_Height / txt_2_Height * 100);

			// scale it
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.MIDDLELEFT);

			// position it
			var newTextX = txt_2_guide_X + (txt_2_guide_Width / 2) - (txt_2_Width / 2);
			var newTextY = txt_2_guide_Y + (txt_2_guide_Height / 2) - (txt_2_Height / 2);

			var deltaX = newTextX - txt_2_X;
			var deltaY = newTextY - txt_2_Y;

			editLayer.translate (deltaX, deltaY); 
		} else {
		    // JUST position it
		    // =======================================================
            // for good measure, re-select frame 2
                var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 2 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );
            
			var newTextX = txt_2_guide_X + (txt_2_guide_Width / 2) - (txt_2_Width / 2);
			var newTextY = txt_2_guide_Y + (txt_2_guide_Height / 2) - (txt_2_Height / 2);

			var deltaX = newTextX - txt_2_X;
			var deltaY = newTextY - txt_2_Y;

			editLayer.translate (deltaX, deltaY); 
		}




	// group_txt_3
	editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];

   // if (editLayer.textItem && editLayer.textItem.contents !== "") {
	   
		// check if txt_3 text is empty (from JSON)
		if (currentBanner.txt_3 !== "") {
			
			newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_3);
			editLayer.textItem.contents = newText;
			editLayer.textItem.color = statesColorsObj[currentState];
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 3 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );
            
            editLayer.textItem.convertToShape();

			var txt_3_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_3_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_3_bounds 	= editLayer.bounds;
			var txt_3_X 		= txt_3_bounds[0].value;
			var txt_3_Y 		= txt_3_bounds[1].value;
		}

		// txt_3_guide position / dimensions
		    // =======================================================
            // for good measure, re-select frame 3
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 3 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );


		editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3_guide"];
			
			var txt_3_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_3_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_3_guide_bounds 	= editLayer.bounds;
			var txt_3_guide_X 		= txt_3_guide_bounds[0].value;
			var txt_3_guide_Y 		= txt_3_guide_bounds[1].value;
	
		// check if txt_3 is taller than guide
		if (txt_3_Height > txt_3_guide_Height) {
			editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
			
			// =======================================================
            // for good measure, re-select frame 3
                var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 3 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );
			
			// get new scale percentage
			newTextScale = Math.floor(txt_3_guide_Height / txt_3_Height * 100);

			// scale it
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.MIDDLELEFT);

			// position it
			var newTextX = txt_3_guide_X + (txt_3_guide_Width / 2) - (txt_3_Width / 2);
			var newTextY = txt_3_guide_Y + (txt_3_guide_Height / 2) - (txt_3_Height / 2);

			var deltaX = newTextX - txt_3_X;
			var deltaY = newTextY - txt_3_Y;

			editLayer.translate (deltaX, deltaY); 
		} else {
		    // JUST position it
		    // =======================================================
            // for good measure, re-select frame 3
                var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 3 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );
            
			var newTextX = txt_3_guide_X + (txt_3_guide_Width / 2) - (txt_3_Width / 2);
			var newTextY = txt_3_guide_Y + (txt_3_guide_Height / 2) - (txt_3_Height / 2);

			var deltaX = newTextX - txt_3_X;
			var deltaY = newTextY - txt_3_Y;

			editLayer.translate (deltaX, deltaY); 
		}




// group_btn
// 
// check for line break "\r"
// 	if (currentBanner.txt_btn.indexOf("\r") > -1) {
// 	
// 			// =======================================================
// 			// select the correct animation frame for the next mods
// 			var idslct = charIDToTypeID( "slct" );
// 				var desc502 = new ActionDescriptor();
// 				var idnull = charIDToTypeID( "null" );
// 					var ref211 = new ActionReference();
// 					var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
// 					ref211.putIndex( idanimationFrameClass, 4 );
// 				desc502.putReference( idnull, ref211 );
// 			executeAction( idslct, desc502, DialogModes.NO );
// 
// 
// 		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_2_lines"];
// 		activeDocument.layerSets["group_btn"].artLayers["txt_btn_2_lines"].visible = true;
// 
// 	} else {
// 	
// 			// =======================================================
// 			// select the correct animation frame for the next mods
// 			var idslct = charIDToTypeID( "slct" );
// 				var desc502 = new ActionDescriptor();
// 				var idnull = charIDToTypeID( "null" );
// 					var ref211 = new ActionReference();
// 					var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
// 					ref211.putIndex( idanimationFrameClass, 4 );
// 				desc502.putReference( idnull, ref211 );
// 			executeAction( idslct, desc502, DialogModes.NO );
// 
// 
// 		// no line break
// 		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_1_line"];
// 		activeDocument.layerSets["group_btn"].artLayers["txt_btn_1_line"].visible = true;
// 
// 	}
// 
// 	//populate text   
// 	if (editLayer.textItem && editLayer.textItem.contents != "") {
//    
// 		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_btn);
// 		editLayer.textItem.contents = newText;
// 		editLayer.textItem.convertToShape();
// 	}

	// group_btn
	editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn"];
    

		// populate text
	    if (editLayer.textItem && editLayer.textItem.contents != "") {
		   
		   // =======================================================
           // select the correct animation frame for the next mods
			var idslct = charIDToTypeID( "slct" );
				var desc502 = new ActionDescriptor();
				var idnull = charIDToTypeID( "null" );
					var ref211 = new ActionReference();
					var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
					ref211.putIndex( idanimationFrameClass, 4 );
				desc502.putReference( idnull, ref211 );
			executeAction( idslct, desc502, DialogModes.NO );
		   
			editLayer.textItem.contents = currentBanner.txt_btn;
			editLayer.textItem.convertToShape();

			var txt_btn_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_btn_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_btn_bounds 	= editLayer.bounds;
			var txt_btn_X 		= txt_btn_bounds[0].value;
			var txt_btn_Y 		= txt_btn_bounds[1].value;
		}

		// txt_btn_guide position / dimensions
		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_guide"];
			
			var txt_btn_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_btn_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_btn_guide_bounds 	= editLayer.bounds;
			var txt_btn_guide_X 		= txt_btn_guide_bounds[0].value;
			var txt_btn_guide_Y 		= txt_btn_guide_bounds[1].value;

		// check if txt_btn is taller than guide
		if (txt_btn_Height > txt_btn_guide_Height) {
			editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn"];
			
			// get new scale percentage
			newTextScale = Math.floor(txt_btn_guide_Height / txt_btn_Height * 100);

			// scale it
		   // =======================================================
           // select the correct animation frame for the next mods
			var idslct = charIDToTypeID( "slct" );
				var desc502 = new ActionDescriptor();
				var idnull = charIDToTypeID( "null" );
					var ref211 = new ActionReference();
					var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
					ref211.putIndex( idanimationFrameClass, 4 );
				desc502.putReference( idnull, ref211 );
			executeAction( idslct, desc502, DialogModes.NO );
		   
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.MIDDLECENTER);

			// position it
			var newTextX = txt_btn_guide_X + (txt_btn_guide_Width / 2) - (txt_btn_Width / 2);
			var newTextY = txt_btn_guide_Y + (txt_btn_guide_Height / 2) - (txt_btn_Height / 2);

			var deltaX = newTextX - txt_btn_X;
			var deltaY = newTextY - txt_btn_Y;
		   // =======================================================
           // select the correct animation frame for the next mods
			var idslct = charIDToTypeID( "slct" );
				var desc502 = new ActionDescriptor();
				var idnull = charIDToTypeID( "null" );
					var ref211 = new ActionReference();
					var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
					ref211.putIndex( idanimationFrameClass, 4 );
				desc502.putReference( idnull, ref211 );
			executeAction( idslct, desc502, DialogModes.NO );

			editLayer.translate (deltaX, deltaY); 
		}
	
	
	// shape_unique_color
	app.activeDocument.activeLayer = activeDocument.layerSets["group_btn"].artLayers["shape_unique_color"];
	// change color of shape
	setColorOfFillLayer( statesColorsObj[currentState] );





	// txt_code
	editLayer = app.activeDocument.artLayers['txt_code'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
    
        if (currentState === "AZ" && currentBanner.language === "ES") {
        newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version + "-S");
        } else {
		newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version);
		}
		editLayer.textItem.contents = newText;

	}


	// txt_legal
	editLayer = app.activeDocument.artLayers['txt_legal'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
	   
		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_legal);
		editLayer.textItem.contents = newText;
	}




	// txt_legal turn on 1 of 2, instead of importing (having problems with keeping on state for all frames not selected below)
	
	// if (currentBanner.language === "EN") {
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_EN"].visible = true;
// 
// 	} else {
// 
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_ES"].visible = true;
// 
// 	}
	


	// THIS MUST BE LAST, OTHERWISE DELETING AN ANIMATION FRAME EARLIER WILL SCREW UP POSITIONS, ETC
	// check if txt_3 text is empty (from JSON) If empty, delete frame
	if (currentBanner.txt_3 === "") {

		editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
		
		// delete frame 3 (0-index)
		deleteFrame(3);

		// remove placeholder text from psd
		editLayer.textItem.contents = '';	

		
	}
	


}


















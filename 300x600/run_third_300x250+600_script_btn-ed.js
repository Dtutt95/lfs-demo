#include "json2.js";
#include "globals.js";

// OUTPUT FOLDER please don't forget to change this or you'll overwrite the previous size
var outputFolder = "/output_300x600";
// var outputFolder = "/output_300x250_Mobile";
   
   
// provide size for file-naming purposes
var currentSize = "300x600_"
// var currentSize = "300x250_M_"


// select the correct data for the template running to override what's in the global file
var dataFile = File(scriptFilePath + "/300_AMB16_data_for-btn-scr.json");



// TIME TO KICK BACK AND TAKE A SNOOZE.
loadData();


// BANNER SPECIFIC ALTERATIONS
function makeAlterations() {

	var newText, editLayer;



	// TURN LOGO ON
	var logoLayer = currentState + '_' + currentBanner.language;
    
            

	
	app.activeDocument.layerSets["group_logos"].layerSets[logoLayer].visible = true;



	// group_txt_1
	if (currentBanner.language === "EN") {
            

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_EN"].visible = true;

	} else {

            

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_ES"].visible = true;

	}
//     NO LONGER NEED to populate TEXT 1, one of two static layers are turned on above!	
//     populate txt 1	
//	if (editLayer.textItem && editLayer.textItem.contents !== "") {

//		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_1);
//		editLayer.textItem.contents = newText;
//		editLayer.textItem.convertToShape();
//
//	}




	// group_txt_2
	editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];

    if (editLayer.textItem && editLayer.textItem.contents != "") {

		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_2);
		editLayer.textItem.contents = newText;
		editLayer.textItem.convertToShape();

	}

		// txt_2 - height, y position
		var txt_2_Height = editLayer.bounds[3] - editLayer.bounds[1];
		var txt_2_Bounds = editLayer.bounds;
		var txt_2_Y = txt_2_Bounds[1].value;





	// group_txt_3
	editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];

    if (editLayer.textItem && editLayer.textItem.contents !== "") {
	   
		// check if txt_3 text is empty (from JSON)
		if (currentBanner.txt_3 !== "") {
			
			newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_3);
			editLayer.textItem.contents = newText;
			editLayer.textItem.color = statesColorsObj[currentState];
            


			// txt_3 - position
			var txt_3_Bounds = editLayer.bounds;
			var txt_3_X = txt_3_Bounds[0].value;
			var txt_3_Y = txt_3_Bounds[1].value;
			
			var newTextX = txt_3_X;
			var newTextY = txt_2_Y + txt_2_Height + 20;

			var deltaX = newTextX - txt_3_X;
			var deltaY = newTextY - txt_3_Y;

			editLayer.translate (deltaX, deltaY); 

		} 
	}




	// group_btn
	editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn"];

    if (editLayer.textItem && editLayer.textItem.contents != "") {   
		   
		   
		   
			editLayer.textItem.contents = currentBanner.txt_btn;
			editLayer.textItem.convertToShape();

			var txt_btn_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_btn_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_btn_bounds 	= editLayer.bounds;
			var txt_btn_X 		= txt_btn_bounds[0].value;
			var txt_btn_Y 		= txt_btn_bounds[1].value;
		}

		// txt_btn_guide position / dimensions
		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_guide"];
			
			var txt_btn_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_btn_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_btn_guide_bounds 	= editLayer.bounds;
			var txt_btn_guide_X 		= txt_btn_guide_bounds[0].value;
			var txt_btn_guide_Y 		= txt_btn_guide_bounds[1].value;

		// check if txt_btn is wider than guide
		if (txt_btn_Width > txt_btn_guide_Width) {
			editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn"];
			
			// get new scale percentage
			newTextScale = Math.floor(txt_btn_guide_Width / txt_btn_Width * 100);

			// scale it
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.MIDDLECENTER);

			// position it
			var newTextX = txt_btn_guide_X + (txt_btn_guide_Width / 2) - (txt_btn_Width / 2);
			var newTextY = txt_btn_guide_Y + (txt_btn_guide_Height / 2) - (txt_btn_Height / 2);

			var deltaX = newTextX - txt_btn_X;
			var deltaY = newTextY - txt_btn_Y;

			editLayer.translate (deltaX, deltaY); 
		} else {
            
		    // or just position it [why does this not work with FIND OUT MORE/get it now/sign up now btns? not wide enought to resize and 'else' does not work?
            
			var newTextX = txt_btn_guide_X + (txt_btn_guide_Width / 2) - (txt_btn_Width / 2);
			var newTextY = txt_btn_guide_Y + (txt_btn_guide_Height / 2) - (txt_btn_Height / 2);

			var deltaX = newTextX - txt_btn_X;
			var deltaY = newTextY - txt_btn_Y;

			editLayer.translate (deltaX, deltaY); 
            
		}

	// shape_unique_color
	app.activeDocument.activeLayer = activeDocument.layerSets["group_btn"].artLayers["shape_unique_color"];
	// change color of shape
	setColorOfFillLayer( statesColorsObj[currentState] );





	// txt_code
	editLayer = app.activeDocument.artLayers['txt_code'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
    
        if (currentState === "AZ" && currentBanner.language === "ES") {
        newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version + "-S");
        } else {
		newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version);
		}
		editLayer.textItem.contents = newText;

	}


	// txt_legal
	editLayer = app.activeDocument.artLayers['txt_legal'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
	   
		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_legal);
		editLayer.textItem.contents = newText;
	}




	// txt_legal turn on 1 of 2, instead of importing (having problems with keeping on state for all frames not selected below)
	
	// if (currentBanner.language === "EN") {
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_EN"].visible = true;
// 
// 	} else {
// 
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_ES"].visible = true;
// 
// 	}
	


	// THIS MUST BE LAST, for static version we're just deleting the subhead here,
	// not deleting an animation frame since this is not animated for 300x600
	if (currentBanner.txt_3 === "") {

		editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
		

		// remove placeholder text from psd
		editLayer.textItem.contents = '';	

		
	}
	
}





















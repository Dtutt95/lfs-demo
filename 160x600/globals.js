// some of these are overwritten by size-specific .js file, but you must
// edit AMB14/AMB15 set mentions below, with proper set and size please!
var scriptFile 			= File($.fileName);

var scriptFilePath 		= scriptFile.path;

var wildcardsFile 		= File(scriptFilePath + "/wildcards.json");

var dataFile 			= File(scriptFilePath + "/data.json");

var wildcardsObj, dataObj, content, docPath, currentBanner, currentState, statesArray;

// WILDCARD VARS
var wildcardState 		= "";
var wildcardLanguage 	= "";
var wildcardMessageID 	= "";

// COLORS
var pink = new SolidColor;
pink.rgb.hexValue = "cb177d";

var green = new SolidColor;
green.rgb.hexValue = "aac932";

var orange = new SolidColor;
orange.rgb.hexValue = "f58220";

var blue = new SolidColor;
blue.rgb.hexValue = "1C4688";


// ASSIGN COLORS TO EACH STATE

var statesColorsObj 	= 
	{ 	
		AR : pink 		,
	 	AZ : pink 		,
	 	CA : orange 	,
	 	FL : orange 	,
	 	GA : orange 	,
	 	IL : pink 		,
	 	IN : pink 		,
	 	KS : orange 	,
	 	LA : pink 		,
	 	MA : orange 	,
	 	MO : pink 		,
	 	MS : orange 	,
	 	NH : blue  		,
	 	OH : green 		,
	 	SC : pink 		,
	 	TX : pink 		,
	 	WA : green 		,
	 	WI : pink 		
	 }



function loadData() {

	// load wildcards

	if (wildcardsFile !== false) {

		// open file
		wildcardsFile.open('r');

		// read file
		content = wildcardsFile.read();

		// parse json
		wildcardsObj = JSON.parse(content);

		// close file
		wildcardsFile.close();

		// check wildcardsObj for "state" variable
		if (wildcardsObj[0].state !== "") {
			//alert(wildcardsObj[0].state);
			wildcardState = wildcardsObj[0].state;
		}

		// check wildcardsObj for "language" variable
		if (wildcardsObj[0].language !== "") {
			//alert(wildcardsObj[0].language);
			wildcardLanguage = wildcardsObj[0].language;
		}

		// check wildcardsObj for "message_id" variable
		if (wildcardsObj[0].message_id !== "") {
			//alert(wildcardsObj[0].message_id);
			wildcardMessageID = wildcardsObj[0].message_id;
		}

	}


	// load main script

	if (dataFile !== false) {

			// open file
			dataFile.open('r');

			// read file
			content = dataFile.read();

			// parse json
			dataObj = JSON.parse(content);

			// print json
			//alert(dataObj.toSource());
			//_print(dataObj.length);

			// close file when done getting info out of it
			dataFile.close();

			// churn 'em out!
			churnOutFiles();

		} else {

			// if something went wrong
			alert("Oh no! Make sure you have all fonts activated.. Or something like that."); 

	}

}




/*

Churn out...Files..GO BACK TO SLEEP

*/
function churnOutFiles() {

	
	// get path of current document
	docPath = app.activeDocument.path;

	
	// loop thru json obj
	for (var i = 0; i < dataObj.length; i ++) {

		currentBanner = dataObj[i];

		// create array from states comma separated string
		statesArray = currentBanner.states.split(",");

		// ***********************
		// WILDCARD language check
		if (wildcardLanguage === "" || wildcardLanguage === currentBanner.language) {

			// ***********************
			// WILDCARD message_id check
			if (wildcardMessageID === "" || wildcardMessageID === 0 || wildcardMessageID === currentBanner.msg_id) {

				// loop thru each state and make gif / jpg
				for (var j = 0; j < statesArray.length; j++) {

					// ***********************
					// WILDCARD state check
					if (wildcardState === "" || wildcardState === statesArray[j]) {

						// duplicate current document so all changes are done on a dummy file
						app.activeDocument.duplicate();

						// set current state
						currentState = statesArray[j];
						
						makeAlterations();

						exportFile(currentBanner);

						// Close duplicate PSD without saving
						app.activeDocument.close(SaveOptions.DONOTSAVECHANGES);

					}					

				}

			}

		}

	}

}



 /* Delete frame --- Yikes! This code is from Photoshop */
function deleteFrame(num) {

	var idslct = charIDToTypeID( "slct" );
	var desc2618 = new ActionDescriptor();
	var idnull = charIDToTypeID( "null" );
	var ref1210 = new ActionReference();
	var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
	ref1210.putIndex( idanimationFrameClass, num );
	desc2618.putReference( idnull, ref1210 );
	executeAction( idslct, desc2618, DialogModes.NO );

	// =======================================================
	var idDlt = charIDToTypeID( "Dlt " );
	var desc2621 = new ActionDescriptor();
	var idnull = charIDToTypeID( "null" );
	var ref1211 = new ActionReference();
	var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
	var idOrdn = charIDToTypeID( "Ordn" );
	var idTrgt = charIDToTypeID( "Trgt" );
	ref1211.putEnumerated( idanimationFrameClass, idOrdn, idTrgt );
	desc2621.putReference( idnull, ref1211 );
	executeAction( idDlt, desc2621, DialogModes.NO );

}




function setColorOfFillLayer( sColor ) {  
    var desc = new ActionDescriptor();  
        var ref = new ActionReference();  
        ref.putEnumerated( stringIDToTypeID('contentLayer'), charIDToTypeID('Ordn'), charIDToTypeID('Trgt') );  
    desc.putReference( charIDToTypeID('null'), ref );  
        var fillDesc = new ActionDescriptor();  
            var colorDesc = new ActionDescriptor();  
            colorDesc.putDouble( charIDToTypeID('Rd  '), sColor.rgb.red );  
            colorDesc.putDouble( charIDToTypeID('Grn '), sColor.rgb.green );  
            colorDesc.putDouble( charIDToTypeID('Bl  '), sColor.rgb.blue );  
        fillDesc.putObject( charIDToTypeID('Clr '), charIDToTypeID('RGBC'), colorDesc );  
    desc.putObject( charIDToTypeID('T   '), stringIDToTypeID('solidColorLayer'), fillDesc );  
    executeAction( charIDToTypeID('setd'), desc, DialogModes.NO );  
}  




function preventWidows(text, lineCharLimit) {

	// prevent widows (orphans)

	var wordsArray = text.split(" ");
	var linesArray = new Array();
	var line = "";

	while (wordsArray.length > 0) {

		while (wordsArray.length > 0 && (line.length + wordsArray[0].length) <= lineCharLimit) {

			line += wordsArray[0] + ' ';
			wordsArray.shift();

		}

		line += "\r";
		linesArray.push(line);
		line = "";

	}

	// check if last line has only one word
	var lastLine = linesArray[linesArray.length-1].split(' ');

	// normally i'd look for 1, but 2 here because the " \r" we add above
	if (lastLine.length === 2) { 
		
		var secondToLastLine = linesArray[linesArray.length-2].split(' ');
		
		// pop off the " \r" first
		secondToLastLine.pop();
		
		var wordToMove = secondToLastLine.pop();
		
		lastLine.unshift("\r" + wordToMove);

		linesArray[linesArray.length-2] = secondToLastLine.join(' ');
		linesArray[linesArray.length-1] = lastLine.join(' ');

	}

	return linesArray.join('');

}



/*

Export  AND MAKE SURE TO EDIT THIS SIZE/AMB15 or AMB14 info for your set!

*/
function exportFile(currentBanner) {

	//alert("Export quality: " + currentBanner.export_quality);
	//alert("Export quality: " + currentBanner.export_format);
	var quality = currentBanner.export_quality;

	var saveFile = File(docPath + outputFolder + '/' + "AMB16_" + currentState + '_banner_' + currentSize + currentBanner.langfile + '_' + currentBanner.theme + '_' + currentBanner.version + currentBanner.date + currentBanner.reward + '.' + currentBanner.export_format);
	var opts = new ExportOptionsSaveForWeb();

	switch (currentBanner.export_format) {

		case "gif":
			
			opts.format = SaveDocumentType.COMPUSERVEGIF;
			opts.includeProfile = false; 
			opts.transparency = false;
			opts.dither = Dither.NONE;
			opts.lossy = 0;
			opts.quality = getGifQuality(quality);

			break;

		case "jpg":

			opts.format = SaveDocumentType.JPEG;  
			opts.includeProfile = false;   
			opts.interlaced = 0;   
			opts.optimized = true;   
			opts.quality = getJpgQuality(quality);

			break;

	}
	
	app.activeDocument.exportDocument(saveFile, ExportType.SAVEFORWEB, opts);

} 




function getGifQuality(num) {

	var result;

	switch (num) {

		case 1:
			result = 100;
			break;

		case 2:
			result = 75;
			break;

		case 3:
			result = 50;
			break;

		case 4:
			result = 25;
			break;

		case 5:
			result = 0;
			break;

	}

	return result;

}




function getJpgQuality(num) {

	var result;

	switch (num) {

		case 1:
			result = 50;
			break;

		case 2:
			result = 60
			break;

		case 3:
			result = 70;
			break;

		case 4:
			result = 85;
			break;

		case 5:
			result = 95;
			break;

	}

	return result;

}


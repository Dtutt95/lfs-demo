#include "json2.js";
#include "globals.js";

// OUTPUT FOLDER please don't forget to change this or you'll overwrite the previous size
 var outputFolder = "/output_160x600";


// select the correct dat for the size to override what's in the global file
var dataFile = File(scriptFilePath + "/160_AMB16_data_normal_1.json");

// provide size for file-naming purposes
var currentSize = "160x600_"


// TIME TO KICK BACK AND TAKE A SNOOZE.
loadData();


// BANNER SPECIFIC ALTERATIONS
function makeAlterations() {

	var newText, editLayer;



	// TURN LOGO ON
	var logoLayer = currentState + '_' + currentBanner.language;
    
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 4 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

	
	app.activeDocument.layerSets["group_logos"].layerSets[logoLayer].visible = true;



	// group_txt_1
	if (currentBanner.language === "EN") {
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 1 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_EN"].visible = true;

	} else {

            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 1 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_ES"].visible = true;

	}
//     NO LONGER NEED to populate TEXT 1, one of two static layers are turned on above!	
//     populate txt 1	
//	if (editLayer.textItem && editLayer.textItem.contents !== "") {

//		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_1);
//		editLayer.textItem.contents = newText;
//		editLayer.textItem.convertToShape();
//
//	}




	// group_txt_2

	editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];

    if (editLayer.textItem && editLayer.textItem.contents != "") {

		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_2);
		editLayer.textItem.contents = newText;
		editLayer.textItem.convertToShape();

        
			var txt_2_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_2_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_2_bounds 	= editLayer.bounds;
			var txt_2_X 		= txt_2_bounds[0].value;
			var txt_2_Y 		= txt_2_bounds[1].value;
		}

		// txt_2_guide position / dimensions
		editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2_guide"];
			
			var txt_2_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_2_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_2_guide_bounds 	= editLayer.bounds;
			var txt_2_guide_X 		= txt_2_guide_bounds[0].value;
			var txt_2_guide_Y 		= txt_2_guide_bounds[1].value;

		// check if txt_2 is wider than guide
		if (txt_2_Width > txt_2_guide_Width) {
			editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];
			
			// get new scale percentage
			newTextScale = Math.floor(txt_2_guide_Width / txt_2_Width * 100);

			// scale it
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.TOPLEFT);

			// position it
			var newTextX = txt_2_guide_X + (txt_2_guide_Width / 2) - (txt_2_Width / 2);
			var newTextY = txt_2_guide_Y + (txt_2_guide_Height / 2) - (txt_2_Height / 2);

			var deltaX = newTextX - txt_2_X;
			var deltaY = newTextY - txt_2_Y;

			editLayer.translate (deltaX, deltaY);
	}

	// txt_2 - height, y position
	var txt_2_Height = editLayer.bounds[3] - editLayer.bounds[1];
	var txt_2_Bounds = editLayer.bounds;
	var txt_2_Y = txt_2_Bounds[1].value;





	// group_txt_3
	editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];

    if (editLayer.textItem && editLayer.textItem.contents !== "") {
	   
		// check if txt_3 text is empty (from JSON)
		if (currentBanner.txt_3 !== "") {
			
			newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_3);
			editLayer.textItem.contents = newText;
			editLayer.textItem.color = statesColorsObj[currentState];

			var txt_3_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_3_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_3_bounds 	= editLayer.bounds;
			var txt_3_X 		= txt_3_bounds[0].value;
			var txt_3_Y 		= txt_3_bounds[1].value;
		}

		// txt_3_guide position / dimensions
		editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3_guide"];
			
			var txt_3_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_3_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_3_guide_bounds 	= editLayer.bounds;
			var txt_3_guide_X 		= txt_3_guide_bounds[0].value;
			var txt_3_guide_Y 		= txt_3_guide_bounds[1].value;

		// check if txt_3 is wider than guide
		if (txt_3_Width > txt_3_guide_Width) {
			editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
			
			// get new scale percentage
			newTextScale = Math.floor(txt_3_guide_Width / txt_3_Width * 100);

			// scale it
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.TOPLEFT);
            
            // =======================================================
            // select the correct animation frame for the next mods
//            var idslct = charIDToTypeID( "slct" );
//                var desc502 = new ActionDescriptor();
//                var idnull = charIDToTypeID( "null" );
//                    var ref211 = new ActionReference();
//                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                    ref211.putIndex( idanimationFrameClass, 3 );
//                desc502.putReference( idnull, ref211 );
//            executeAction( idslct, desc502, DialogModes.NO );
//

			// txt_3 - position
			editLayer_text_2 = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];
			editLayer_text_3 = editLayer;
			text_3_bounds = editLayer_text_3.bounds;
			text_2_bounds = editLayer_text_2.bounds;

			text_3_top_x = text_3_bounds[0];
			text_3_top_y = text_3_bounds[1];
			text_3_bottom_x = text_3_bounds[2];
			text_3_bottom_y = text_3_bounds[3];

			text_2_top_x = text_2_bounds[0];
			text_2_top_y = text_2_bounds[1];
			text_2_bottom_x = text_2_bounds[2];
			text_2_bottom_y = text_2_bounds[3];

			text_2_height = text_2_bottom_y - text_2_top_y
			text_3_transform_y = new UnitValue(-25 + (text_2_height.as("pixels") - 47), "pixels")

			editLayer.translate (0, text_3_transform_y);


            
            // make text active layer and position it
            // activeLayer = activeDocument.activeLayer;
//            editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];
//
//
//            	/////////////////////////////////////////////////////////////////////////////////////////
//	            /// * MIKE's Addition
//
//	            editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];
//	            txt_2_Width 	= editLayer.bounds[3] - editLayer.bounds[1];
//				txt_2_Height 	= editLayer.bounds[2] - editLayer.bounds[0];
//				txt_2_bounds 	= editLayer.bounds;
//				txt_2_X 		= txt_2_bounds[0].value;
//				txt_2_Y 		= txt_2_bounds[1].value;
//
//            	newTextY = txt_2_Y + txt_2_Height + 20;
//            
//	        	editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
//
//
//            	/// * end MIKE's Addition
//            	/////////////////////////////////////////////////////////////////////////////////////////
//            var newTextX = 5;
//
//            var deltaX = newTextX - txt_3_X;
//            var deltaY = newTextY - txt_3_Y;
//
//            editLayer.translate (deltaX, deltaY);

            
            // get dimensions and position of text 2
//            activeLayer = activeDocument.layerSets["group_txt_2"].artLayers['txt_2'];
//            var activeLayer = activeDocument.activeLayer;
//
//            var txt_2_Width = activeLayer.bounds[2]-activeLayer.bounds[0];
//            var txt_2_Height = activeLayer.bounds[3]-activeLayer.bounds[1];
//            var txt_2_Bounds = activeLayer.bounds;
//            var txt_2_X = txt_2_Bounds[0].value;
//            var txt_2_Y = txt_2_Bounds[1].value;

            // make text 3 active layer and position it
//            activeLayer = activeDocument.layerSets['group_txt_3'].artlayers['txt_3'];
//
//            var newTextX = 5;
//            var newTextY = txt_2_Bounds[1].value + 20;
//
//            var deltaX = newTextX - textX;
//            var deltaY = newTextY - textY;
//
//            activeLayer.translate (deltaX, deltaY); 


			// position it
			// var newTextX = txt_3_guide_X + (txt_3_guide_Width / 2) - (txt_3_Width / 2);
			// var newTextY = txt_3_guide_Y + (txt_3_guide_Height / 2) - (txt_3_Height / 2);
            // var newTextY = txt_2_Y + txt_2_Height + 20;

			//var deltaX = newTextX - txt_3_X;
			// var deltaY = newTextY //- txt_3_Y;

			// editLayer.translate (deltaX, deltaY);


			// txt_3 - position
//			var txt_3_Bounds = editLayer.bounds;
//			var txt_3_X = txt_3_Bounds[0].value;
//			var txt_3_Y = txt_3_Bounds[1].value;
//			
//			var newTextX = txt_3_X;
//			var newTextY = txt_2_Y + txt_2_Height + 20;


		} 
	}




	// group_btn
	editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn"];

    if (editLayer.textItem && editLayer.textItem.contents != "") {   
		   
		   // =======================================================
           // select the frame to activate for these mods, holding visible txt_btn
			var idslct = charIDToTypeID( "slct" );
				var desc502 = new ActionDescriptor();
				var idnull = charIDToTypeID( "null" );
					var ref211 = new ActionReference();
					var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
					ref211.putIndex( idanimationFrameClass, 4 );
				desc502.putReference( idnull, ref211 );
			executeAction( idslct, desc502, DialogModes.NO );
		   
			editLayer.textItem.contents = currentBanner.txt_btn;
			editLayer.textItem.convertToShape();

			var txt_btn_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_btn_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_btn_bounds 	= editLayer.bounds;
			var txt_btn_X 		= txt_btn_bounds[0].value;
			var txt_btn_Y 		= txt_btn_bounds[1].value;
		}

		// txt_btn_guide position / dimensions
		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_guide"];
			
			var txt_btn_guide_Width 	= editLayer.bounds[2] - editLayer.bounds[0];
			var txt_btn_guide_Height 	= editLayer.bounds[3] - editLayer.bounds[1];
			var txt_btn_guide_bounds 	= editLayer.bounds;
			var txt_btn_guide_X 		= txt_btn_guide_bounds[0].value;
			var txt_btn_guide_Y 		= txt_btn_guide_bounds[1].value;

		// check if txt_btn is wider than guide
		if (txt_btn_Width > txt_btn_guide_Width) {
			editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn"];
			
			// get new scale percentage
			newTextScale = Math.floor(txt_btn_guide_Width / txt_btn_Width * 100);

			// scale it
			editLayer.resize(newTextScale, newTextScale, AnchorPosition.MIDDLECENTER);

			// position it
			var newTextX = txt_btn_guide_X + (txt_btn_guide_Width / 2) - (txt_btn_Width / 2);
			var newTextY = txt_btn_guide_Y + (txt_btn_guide_Height / 2) - (txt_btn_Height / 2);

			var deltaX = newTextX - txt_btn_X;
			var deltaY = newTextY - txt_btn_Y;

			editLayer.translate (deltaX, deltaY); 
		} else {
            
		    // or just position it [why does this not work with FIND OUT MORE/get it now/sign up now btns? not wide enought to resize and 'else' does not work?
            
			var newTextX = txt_btn_guide_X + (txt_btn_guide_Width / 2) - (txt_btn_Width / 2);
			var newTextY = txt_btn_guide_Y + (txt_btn_guide_Height / 2) - (txt_btn_Height / 2);

			var deltaX = newTextX - txt_btn_X;
			var deltaY = newTextY - txt_btn_Y;

			editLayer.translate (deltaX, deltaY); 
            
		}
	
	
	// shape_unique_color
	app.activeDocument.activeLayer = activeDocument.layerSets["group_btn"].artLayers["shape_unique_color"];
	// change color of shape
	setColorOfFillLayer( statesColorsObj[currentState] );


	// txt_code
	editLayer = app.activeDocument.artLayers['txt_code'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
    
        if (currentState === "AZ" && currentBanner.language === "ES") {
        newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version + "-S");
        } else {
		newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version);
		}
		editLayer.textItem.contents = newText;

	}


	// txt_legal
	editLayer = app.activeDocument.artLayers['txt_legal'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
	   
		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_legal);
		editLayer.textItem.contents = newText;
	}




	// txt_legal turn on 1 of 2, instead of importing (having problems with keeping on state for all frames not selected below)
	
	// if (currentBanner.language === "EN") {
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_EN"].visible = true;
// 
// 	} else {
// 
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_ES"].visible = true;
// 
// 	}
	


	// THIS MUST BE LAST, OTHERWISE DELETING AN ANIMATION FRAME EARLIER WILL SCREW UP POSITIONS, ETC
	// check if txt_3 text is empty (from JSON) If empty, delete frame
	if (currentBanner.txt_3 === "") {

		editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
		
		// delete frame 3 (0-index)
		deleteFrame(2);

		// remove placeholder text from psd
		editLayer.textItem.contents = '';	

		
	}
	
}





















to get all 300x250 banners run, you'll need to run 2 templates with an edited script:
comment and un-comment referenced data file lines in lines 13 through 15 of script
"300x250+600_script.js" to run both in this order (#2 will overwrite only 4 files):

#1 run cen-300x250-Mobile-templ_1.psd with data 300_AMB16_data_normal_1.json
#2 run cen-300x250-Mobile-templ-smaller-btn_2.psd with 300_AMB16_data_smaller-btn_2.json

To get here, I first ran all of one state's banners and reviewed.

For first run template

- added line breaks (\r) to any button copy that occupied two lines. 
Presence of \r triggers a different resize script to fit and position two-line buttons.

- strategically broke headlines and subheads to fix orphans - this was done after
running all of one state's messages without line breaks, (re-do any time a new json file 
is being used). Then identified the headlines/subheads that need manual line breaks, and 
insert \r everywhere line break is desired.
_________

Second run template #2 fixed buttons that were too large... button text box was 
widened and headline/subhead text was made 2 pts smaller for 181-115 SP, to fit)

- ran with the template 2 which fixed all fitting problems on 4 banners.
_________

* button size issue could be mitigated in the future if we can fit things vertically
as well as horizontally in the fit-size script (pick the resize that is most severe 
of the 2). Right now it looks like the resize script vert and horiz resize items only 
seem to take into account one dimension. May need AND-to-OR change to fix?
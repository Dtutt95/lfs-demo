#include "json2.js";
#include "globals.js";

// OUTPUT FOLDER please don't forget to change this or you'll overwrite the previous size
// var outputFolder = "/output_300x600_Display";
var outputFolder = "/output_300x250_Mobile";
   
   
// provide size for file-naming purposes
// var currentSize = "300x250_D_"
var currentSize = "300x250_M_"


// select the correct data for the size to override what's in the global file
// var dataFile = File(scriptFilePath + "/300_AMB16_data_normal_1.json");
var dataFile = File(scriptFilePath + "/300_AMB16_data_smaller-btn_2.json");



// TIME TO KICK BACK AND TAKE A SNOOZE.
loadData();


// BANNER SPECIFIC ALTERATIONS
function makeAlterations() {

	var newText, editLayer;



	// TURN LOGO ON
	var logoLayer = currentState + '_' + currentBanner.language;
    
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 4 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

	
	app.activeDocument.layerSets["group_logos"].layerSets[logoLayer].visible = true;



	// group_txt_1
	if (currentBanner.language === "EN") {
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 1 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_EN"].visible = true;

	} else {

            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 1 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		editLayer = app.activeDocument.layerSets["group_txt_1"].artLayers["txt_1_ES"].visible = true;

	}
//     NO LONGER NEED to populate TEXT 1, one of two static layers are turned on above!	
//     populate txt 1	
//	if (editLayer.textItem && editLayer.textItem.contents !== "") {

//		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_1);
//		editLayer.textItem.contents = newText;
//		editLayer.textItem.convertToShape();
//
//	}




	// group_txt_2
	editLayer = activeDocument.layerSets["group_txt_2"].artLayers["txt_2"];

    if (editLayer.textItem && editLayer.textItem.contents != "") {

		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_2);
		editLayer.textItem.contents = newText;
		editLayer.textItem.convertToShape();

	}

		// txt_2 - height, y position
		var txt_2_Height = editLayer.bounds[3] - editLayer.bounds[1];
		var txt_2_Bounds = editLayer.bounds;
		var txt_2_Y = txt_2_Bounds[1].value;





	// group_txt_3
	editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];

    if (editLayer.textItem && editLayer.textItem.contents !== "") {
	   
		// check if txt_3 text is empty (from JSON)
		if (currentBanner.txt_3 !== "") {
			
			newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_3);
			editLayer.textItem.contents = newText;
			editLayer.textItem.color = statesColorsObj[currentState];
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 3 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );


			// txt_3 - position
			var txt_3_Bounds = editLayer.bounds;
			var txt_3_X = txt_3_Bounds[0].value;
			var txt_3_Y = txt_3_Bounds[1].value;
			
			var newTextX = txt_3_X;
			var newTextY = txt_2_Y + txt_2_Height + 20;

			var deltaX = newTextX - txt_3_X;
			var deltaY = newTextY - txt_3_Y;

			editLayer.translate (deltaX, deltaY); 

		} 
	}




	// group_btn

//    check for line break "\r"
 	  if (currentBanner.txt_btn.indexOf("\r") > -1) {
         
             // =======================================================
             // select the correct animation frame for the next mods
             var idslct = charIDToTypeID( "slct" );
                 var desc502 = new ActionDescriptor();
                 var idnull = charIDToTypeID( "null" );
                     var ref211 = new ActionReference();
                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                     ref211.putIndex( idanimationFrameClass, 4 );
                 desc502.putReference( idnull, ref211 );
             executeAction( idslct, desc502, DialogModes.NO );
 
 
 		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_2_lines"];
 		activeDocument.layerSets["group_btn"].artLayers["txt_btn_2_lines"].visible = true;
// 
 	} else {
//         
            // =======================================================
            // select the correct animation frame for the next mods
            var idslct = charIDToTypeID( "slct" );
                var desc502 = new ActionDescriptor();
                var idnull = charIDToTypeID( "null" );
                    var ref211 = new ActionReference();
                    var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
                    ref211.putIndex( idanimationFrameClass, 4 );
                desc502.putReference( idnull, ref211 );
            executeAction( idslct, desc502, DialogModes.NO );

		// no line break
		editLayer = activeDocument.layerSets["group_btn"].artLayers["txt_btn_1_line"];
		activeDocument.layerSets["group_btn"].artLayers["txt_btn_1_line"].visible = true;

	}

    //populate text   
    if (editLayer.textItem && editLayer.textItem.contents != "") {
	   
		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_btn);
		editLayer.textItem.contents = newText;
		editLayer.textItem.convertToShape();
	}

	// shape_unique_color
	app.activeDocument.activeLayer = activeDocument.layerSets["group_btn"].artLayers["shape_unique_color"];
	// change color of shape
	setColorOfFillLayer( statesColorsObj[currentState] );





	// txt_code
	editLayer = app.activeDocument.artLayers['txt_code'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
    
        if (currentState === "AZ" && currentBanner.language === "ES") {
        newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version + "-S");
        } else {
		newText = editLayer.textItem.contents.replace (/.+/, "AMB16" + "-" + currentState + "-" + "C-00" + currentBanner.version);
		}
		editLayer.textItem.contents = newText;

	}


	// txt_legal
	editLayer = app.activeDocument.artLayers['txt_legal'];

    if (editLayer.textItem && editLayer.textItem.contents != "") {
	   
		newText = editLayer.textItem.contents.replace (/.+/, currentBanner.txt_legal);
		editLayer.textItem.contents = newText;
	}




	// txt_legal turn on 1 of 2, instead of importing (having problems with keeping on state for all frames not selected below)
	
	// if (currentBanner.language === "EN") {
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_EN"].visible = true;
// 
// 	} else {
// 
//             // =======================================================
//             // select the correct animation frame for the next mods
//             var idslct = charIDToTypeID( "slct" );
//                 var desc502 = new ActionDescriptor();
//                 var idnull = charIDToTypeID( "null" );
//                     var ref211 = new ActionReference();
//                     var idanimationFrameClass = stringIDToTypeID( "animationFrameClass" );
//                     ref211.putIndex( idanimationFrameClass, 1 );
//                 desc502.putReference( idnull, ref211 );
//             executeAction( idslct, desc502, DialogModes.NO );
// 
// 		app.activeDocument.layerSets["group_txt_legal"].artLayers["txt_legal_ES"].visible = true;
// 
// 	}
	


	// THIS MUST BE LAST, OTHERWISE DELETING AN ANIMATION FRAME EARLIER WILL SCREW UP POSITIONS, ETC
	// check if txt_3 text is empty (from JSON) If empty, delete frame
	if (currentBanner.txt_3 === "") {

		editLayer = activeDocument.layerSets["group_txt_3"].artLayers["txt_3"];
		
		// delete frame 3 (0-index)
		deleteFrame(2);

		// remove placeholder text from psd
		editLayer.textItem.contents = '';	

		
	}
	
}




















